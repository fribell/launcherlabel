[![Android Arsenal](https://img.shields.io/badge/Android%20Arsenal-Launcher--Label-green.svg?style=flat)](https://android-arsenal.com/details/1/2255)

Launcher-Label
==============
Android gradle plugin that adds text to all the debug launcher icons. That is useful for QA teams to know the version of the app that are testing.

![Screen Shot 2015-08-06 at 14.43.49.png](https://bitbucket.org/repo/RKEBq8/images/4035637473-Screen%20Shot%202015-08-06%20at%2014.43.49.png)

Getting started
---------------

**Dependency**

Initialises the plugin in build.gradle
```groovy
buildscript {
    repositories {
        mavenCentral()
    }

    dependencies {
        classpath 'com.ribell:launcher-label:1.0.1'
    }
}

apply plugin: 'com.android.application'
apply plugin: 'launcher-label'

buildTypes {
        release {
            debuggable false
        }

        debug {
            debuggable true
        }
    }
```
Remember to add "debuggable true" in the buildTypes that you want modify the launcher

**Default values**

You can setup general values if you don't like the default ones
```groovy
launcherLabel {
    fontSize = 20
    fontColor = "#000000"
    bgColor = "#FFFFFF"
    iconName = "ic_launcher.png"
}
```

You can setup specific text for each flavour. Each string in the array is one line.
```groovy
productFlavors {
        prod {
            applicationId "com.ribell.hangdroidcast"
            signingConfig signingConfigs.release
        }
        qa {
            applicationId "com.ribell.hangdroidcast"
            signingConfig signingConfigs.qa
            versionName = android.defaultConfig.versionName + " (QA) "+getVerCode()
            ext.labels = [
                    "QA "+new Date().getDateString(),
                    getVerName()+" ("+getVerCode()+")",
                    "Test"
                ]
        }
    }
```

**Run plugin**

The plugin run automatically, so you don't have to call any task

License
--------

Copyright 2015 Ferran Ribell

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.