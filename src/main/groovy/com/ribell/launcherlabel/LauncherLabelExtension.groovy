package com.ribell.launcherlabel
import com.google.common.collect.Lists
import groovy.io.FileType

import javax.imageio.ImageIO
import java.awt.*
import java.awt.image.BufferedImage
import java.awt.image.ColorConvertOp
import java.util.List

import static java.awt.Color.BLACK
import static java.awt.Color.WHITE

class LauncherLabelExtension {
    int fontSize
    String fontColor
    String bgColor
    String iconName
    String[] labels

    static List<File> searchIcons(File where, String iconName = "ic_launcher.png") {
        List<File> result = Lists.newArrayList();
        where.eachFileRecurse(FileType.FILES) { File file ->
            if (file.name == iconName) {
                result.add(file)
            }
        }

        result
    }

    static void addLabels(int fontSize = 12, File image, Color bgColor = WHITE, Color fontColor = BLACK, String[] lines) {
        int distance = 2

        final BufferedImage originalImage = ImageIO.read(image);
        BufferedImage bufferedImage = new BufferedImage(originalImage.getWidth(), originalImage.getHeight(),
                BufferedImage.TYPE_3BYTE_BGR);

        ColorConvertOp op = new ColorConvertOp(null);
        op.filter(originalImage, bufferedImage);

        Graphics2D mainGraphic =  (Graphics2D) bufferedImage.getGraphics();

        mainGraphic.setColor(bgColor)
        mainGraphic.fillRect(0, bufferedImage.height - (fontSize * lines.size()), bufferedImage.width, bufferedImage.height - (fontSize * lines.size()))

        Font font = new Font("Verdana", Font.PLAIN, fontSize);
        mainGraphic.setFont(font);
        mainGraphic.setColor(fontColor)

        lines.eachWithIndex { String line, int i ->
            mainGraphic.drawString(line, distance, bufferedImage.height - distance - ((distance + fontSize) * (lines.size()-i-1)));
        }

        ImageIO.write(bufferedImage, "png", image);
    }
}
