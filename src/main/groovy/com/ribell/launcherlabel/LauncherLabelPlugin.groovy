package com.ribell.launcherlabel
import com.android.build.gradle.AppPlugin
import com.android.build.gradle.api.BaseVariant
import com.android.build.gradle.api.BaseVariantOutput
import com.android.build.gradle.tasks.ProcessAndroidResources
import org.gradle.api.Plugin
import org.gradle.api.Project

import java.awt.*

import static com.ribell.launcherlabel.LauncherLabelExtension.searchIcons
import static com.ribell.launcherlabel.LauncherLabelExtension.addLabels

class LauncherLabelPlugin implements Plugin<Project> {

    static {
        System.setProperty("java.awt.headless", "true")

        // workaround for an Android Studio issue
        try {
            Class.forName(System.getProperty("java.awt.graphicsenv"))
        } catch (ClassNotFoundException e) {
            System.err.println("[WARN] java.awt.graphicsenv: " + e)
            System.setProperty("java.awt.graphicsenv", "sun.awt.CGraphicsEnvironment")
        }
        try {
            Class.forName(System.getProperty("awt.toolkit"))
        } catch (ClassNotFoundException e) {
            System.err.println("[WARN] awt.toolkit: " + e)
            System.setProperty("awt.toolkit", "sun.lwawt.macosx.LWCToolkit")
        }
    }

    @Override
    void apply(Project project) {
        if (!project.plugins.hasPlugin(AppPlugin)) {
            throw new IllegalStateException("'android' plugin is required.")
        }

        project.extensions.create('launcherLabel', LauncherLabelExtension)
        project.task('launcherLabelTask') << {
            def log = project.logger
            def variants = project.android.applicationVariants
            int fontSize = project.launcherLabel.fontSize ? project.launcherLabel.fontSize : 20
            String fontColor = project.launcherLabel.fontColor ? project.launcherLabel.fontColor : "#000000"
            String bgColor = project.launcherLabel.bgColor ? project.launcherLabel.bgColor : "#FFFFFF"
            String iconName = project.launcherLabel.iconName ? project.launcherLabel.iconName : "ic_launcher.png"
            log.info "Start launcher label plugin"

            variants.all { BaseVariant variant ->

                if (!variant.buildType.debuggable) {
                    log.info "LauncherLabelPlugin. Is not debuggable variant: $variant.name"
                    return
                }

                log.info "LauncherLabelPlugin. Variant in process: $variant.name"
                variant.outputs.each { BaseVariantOutput output ->
                    output.processResources.doFirst {
                        ProcessAndroidResources task = delegate
                        variant.outputs.each { BaseVariantOutput variantOutput ->
                            File resDir = task.resDir
                            log.info "Icon file location: $resDir.absolutePath"

                            searchIcons(resDir, iconName).each { File icon ->
                                log.info "Adding labels in the icon: " + icon.absolutePath

                                def buildName = variant.flavorName + " " + variant.buildType.name
                                def version = variant.mergedFlavor.versionName
                                String[] labels = [buildName, version]
                                if(variant.productFlavors.get(0).hasProperty('labels')){
                                    labels = variant.productFlavors.get(0).ext.labels
                                }

                                addLabels(fontSize, icon, Color.decode(bgColor), Color.decode(fontColor), labels)
                            }
                        }
                    }
                }
            }
        }

        project.tasks.launcherLabelTask.execute()

    }
}

